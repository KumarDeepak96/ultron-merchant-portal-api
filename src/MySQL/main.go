package MySQL

import (
	_ "github.com/go-sql-driver/mysql"
	"github.com/jinzhu/gorm"
	log "github.com/sirupsen/logrus"
	"os"
	"sync"
)

var (
	once                     sync.Once
	Mysqldb                  *gorm.DB
	CommunicationDB          *gorm.DB
	mysqlHost                = os.Getenv("MYSQL_HOST")
	mysqlUserName            = os.Getenv("MYSQL_USERNAME")
	mysqlPassword            = os.Getenv("MYSQL_PASSWORD")
	mysqlDefaultSchema       = os.Getenv("MYSQL_DEFAULT_SCHEMA")
	mysqlCommunicationSchema = "communication"
)

func init() {
	var err error
	dbDSN := mysqlUserName + ":" + mysqlPassword + "@tcp(" + mysqlHost + ":3306)/" + mysqlDefaultSchema + "?charset=utf8&parseTime=True&loc=Local"
	Mysqldb, err = gorm.Open("mysql", dbDSN)
	if err != nil {
		log.WithFields(log.Fields{
			"err": err,
		}).Error("error while making mysql connection")
	}
	Mysqldb.DB().SetMaxOpenConns(10)
	Mysqldb.DB().SetMaxIdleConns(2)
	dbDSN = mysqlUserName + ":" + mysqlPassword + "@tcp(" + mysqlHost + ":3306)/" + mysqlCommunicationSchema + "?charset=utf8&parseTime=True&loc=Local"
	CommunicationDB, err = gorm.Open("mysql", dbDSN)
	if err != nil {
		log.WithFields(log.Fields{
			"err": err,
		}).Error("error while making mysql connection")
	}
	CommunicationDB.DB().SetMaxOpenConns(10)
	CommunicationDB.DB().SetMaxIdleConns(2)
}
