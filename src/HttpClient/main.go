package HttpClient

import (
	log "github.com/sirupsen/logrus"
	"net/http"
	"os"
	"strconv"
	"sync"
	"time"
)

var (
	hc           *HttpClient
	MaxIdleConns = os.Getenv("MAX_IDLE_CONNS")
	once         sync.Once
)

type HttpClient struct {
	Client *http.Client
}

func NewClient() *HttpClient {
	hc := HttpClient{}
	// Customize the Transport to have larger connection pool
	defaultRoundTripper := http.DefaultTransport
	defaultTransportPointer, ok := defaultRoundTripper.(*http.Transport)
	if !ok {
		log.Errorf("defaultRoundTripper not an *http.Transport - Host failed: %s", ok)

	}
	defaultTransport := *defaultTransportPointer
	MaxIdleConns, err := strconv.Atoi(MaxIdleConns)
	if err != nil {
		MaxIdleConns = 1000
	}
	defaultTransport.MaxIdleConns = MaxIdleConns
	defaultTransport.MaxIdleConnsPerHost = MaxIdleConns
	hc.Client = &http.Client{
		Timeout:   2 * time.Second,
		Transport: &defaultTransport}

	return &hc
}

func Instance() *HttpClient {
	once.Do(func() {
		hc = NewClient()
	})
	return hc
}
