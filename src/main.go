package main

import (
	"encoding/json"
	"github.com/gorilla/mux"
	log "github.com/sirupsen/logrus"
	"net/http"
	"os"
	"strings"
	"ultron-merchant-portal/Cassandra"
	"ultron-merchant-portal/DataDog"
)

var (
	Port     = os.Getenv("UIG_API_PORT")
	logLevel = os.Getenv("LOG_LEVEL")
	cql      = Cassandra.Instance()
)

//Middleware should be able to handle panic
func recoverHandler(next http.Handler) http.Handler {
	dd := DataDog.Instance()
	fn := func(w http.ResponseWriter, r *http.Request) {
		defer func() {
			if err := recover(); err != nil {
				//Middleware should be able to handle panic

				log.WithFields(log.Fields{
					"URL": r.RequestURI,
				}).Errorf("panic: %+v", err)
				dd.Count("status_500", 1, strings.Fields(""), 1)
				http.Error(w, http.StatusText(500), 500)
			}
		}()
		next.ServeHTTP(w, r)
	}
	return http.HandlerFunc(fn)
}

//Middleware should be able to call dd metric
func ddMetricHandler(next http.Handler) http.Handler {
	dd := DataDog.Instance()
	fn := func(w http.ResponseWriter, r *http.Request) {
		defer func() {
			relativePath := "uig:path_" + r.URL.Path
			dd.Count("uig.apis", 1, strings.Fields(relativePath), 1)
		}()
		next.ServeHTTP(w, r)
	}
	return http.HandlerFunc(fn)
}

func main() {
	//var cpuprofile = flag.String("cpuprofile", "/tmp/a.pprof", "write cpu profile to file")
	//flag.Parse()
	//if *cpuprofile != "" {
	//	log.Infof("creating cpu profile")
	//	f, err := os.Create(*cpuprofile)
	//	if err != nil {
	//		log.Infof("error while creating file")
	//		log.Fatal(err)
	//	}
	//	pprof.StartCPUProfile(f)
	//	defer pprof.StopCPUProfile()
	//}

	//defer profile.Start(profile.CPUProfile).Stop()

	level, err := log.ParseLevel(logLevel)
	if err != nil {
		level = log.ErrorLevel
	}

	log.SetOutput(os.Stdout)
	log.SetLevel(level)

	log.WithFields(log.Fields{
		"port": Port,
	}).Info("Starting UIG API on port")

	//CassandraSession := Cassandra.Session
	//defer CassandraSession.Close()

	router := mux.NewRouter().StrictSlash(true)
	router.Use(ddMetricHandler)
	router.Use(recoverHandler)
	addr := ":" + Port
	log.Info(Port)
	log.Info(addr)
	errHttp := http.ListenAndServe(addr, router) // setting listening port

	if errHttp != nil {
		log.WithFields(log.Fields{
			"addr": addr,
			"err":  errHttp,
		}).Fatal("Unable to create HTTP Service ")
	}
}

type heartbeatResponse struct {
	Status string `json:"status"`
	Code   int    `json:"code"`
}

// URL : /
func heartbeat(w http.ResponseWriter, r *http.Request) {
	_ = json.NewEncoder(w).Encode(heartbeatResponse{Status: "OK", Code: 200})
}
