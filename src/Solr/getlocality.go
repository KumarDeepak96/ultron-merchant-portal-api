package Solr

import (
	//log "github.com/sirupsen/logrus"
	"../DataDog"
	//"in.magicpin/golang/uig-api/Locality"
	log "github.com/sirupsen/logrus"
	"strings"
)

type Locality struct {
	ID       int    `json:"id"`
	Name     string `json:"name"`
	City     string `json:"city"`
	Cluster  string `json:"cluster"`
	MegaCity string `json:"mega_city"`
}

func GetLocalityId(num int, latlon string) []Locality {
	var localities []Locality
	DDAgent := DataDog.Instance()

	results, err := ExecuteResolveLatLongQuery(num, latlon)
	if err != nil {
		DDAgent.Count("solr.request.getlocality", 1, strings.Fields("error"), 1)
		log.Errorf("Solr Get Locatily failed : %s %s", err, latlon)
		return localities
	}

	for _, document := range results.Documents {
		var locality Locality
		locality = Locality{
			ID:       document.LID,
			Name:     document.Locality,
			City:     document.City,
			MegaCity: document.MegaCity,
			Cluster:  document.Cluster,
		}
		log.WithFields(log.Fields{
			"ID":        locality.ID,
			"Name":      locality.Name,
			"City":      locality.City,
			"Mega City": locality.MegaCity,
			"Cluster":   locality.Cluster,
		}).Debug("Locality Fetched ")
		localities = append(localities, locality)
	}
	DDAgent.Count("solr.request.getlocality", 1, strings.Fields("success"), 1)

	return localities
}
