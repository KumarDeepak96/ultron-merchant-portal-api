package Solr

import (
	"context"
	log "github.com/sirupsen/logrus"
	"../DataDog"
	"strings"
)

//http://g2.gc.magicpin.in:8983/solr/deals/
//select?q=search_text:*&wt=json&f
//q=deal_visibility:VISIBLE&fq=is_campaign:false&indent=true&rows=10&
//fq=custom_tags:HeavenlyBurgers&fq=locality_id:11&fq=rating:[4%20TO%20*]

type User struct {
	ID           int                `json:"id"`
	Popularity   int                `json:"popularity"`
	User_Type    string             `json:"user_type"`
	Locality_Id  int                `json:"locality_Id"`
	Interests    map[string]float32 `json:"interests"`
	Locality     []string           `json:"locality"`
	TopInterests []string           `json:"top_interests"`
	TxnIds       []int              `json:"tnx_ids"`
}

func GetMerchants(ctx context.Context, num int, locality_id int, customTag string) interface{} {
	var users []User
	DDAgent := DataDog.Instance()

	results, err := ExecuteQuery(ctx, num, locality_id, customTag)
	if err != nil {
		return users
	}

	for _, document := range results.Documents {
		var user User
		user = User{
			ID:          document.MerchantID,
			User_Type:   "merchant",
			Locality_Id: document.LocalityID,
		}
		users = append(users, user)
	}
	DDAgent.Count("solr.request.Success", 1, strings.Fields(""), 1)
	return users
}

func GetMerchantsLocalityCentered(ctx context.Context, num int, locality_id int, customTag string) interface{} {
	
	var users []User
	DDAgent := DataDog.Instance()

	results, err := ExecuteGEOQuery(ctx, locality_id)
	if err != nil {
		return users
	}
	var latlon string
	for _, document := range results.Documents {
		latlon = document.LatLon
	}
	if latlon == "" {
		log.WithFields(log.Fields{
			"locality_id": locality_id,
		}).Error("Couldnt fetch Lat Lon for locality_id")
		DDAgent.Count("solr.request.Error", 1, strings.Fields("GEO Query"), 1)
		return users
	}
	results, err = ExecuteLocalityCenteredQuery(ctx, num, latlon, customTag)
	if err != nil {
		return users
	}
	for _, document := range results.Documents {
		var user User
		user = User{
			ID:          document.MerchantID,
			User_Type:   "merchant",
			Locality_Id: document.LocalityID,
		}
		users = append(users, user)
	}
	DDAgent.Count("solr.request.Success", 1, strings.Fields(""), 1)
	return users
}

func GetMerchantsNoLocality(num int, customTag string) []User {
	var users []User
	DDAgent := DataDog.Instance()

	results, err := ExecuteQueryNoLocality(num, customTag)
	if err != nil {
		return users
	}

	for _, document := range results.Documents {
		var user User
		user = User{
			ID:          document.MerchantID,
			User_Type:   "merchant",
			Locality_Id: document.LocalityID,
		}
		users = append(users, user)
	}
	DDAgent.Count("solr.request.Success", 1, strings.Fields(""), 1)
	return users
}
