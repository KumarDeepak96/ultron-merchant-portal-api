package Solr

import (
	"context"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/url"
	"os"
	"strconv"
	"strings"
	"time"

	"gopkg.in/DataDog/dd-trace-go.v1/ddtrace/tracer"
	"github.com/saikiran-magic/go-solr"
	log "github.com/sirupsen/logrus"
	"../DataDog"
	"../HttpClient"
)

type Document struct {
	MerchantID   int    `json:"user_id"`
	LocalityCity string `json:"locality_city"`
	LocalityID   int    `json:"locality_id"`
	LatLon       string `json:"latlon"`
	//Added fields for merchants widgets response
	MerchantCoverPic    string  `json:"merchant_cover_pic"`
	MerchantLogo        string  `json:"merchant_logo"`
	MerchantName        string  `json:"merchant_name"`
	MerchantDesc        string  `json:"merchant_desc"`
	Locality            string  `json:"locality"`
	Rating              float64 `json:"rating"`
	DealID              int     `json:"deal_id"`
	ProductName         string  `json:"product_name"`
	GlobalCashbackValue string  `json:"global_cashback_value"`
	CostForTwo          float64 `json:"cost_for_two"`
	CDNUrl              string  `json:"cdn_url"`

	//Voucher field
	RedemptionOptionID                int      `json:"redemption_option_id"`
	RedemptionOptionAmountID          []string `json:"redemption_option_amount_id"`
	VoucherType                       []string `json:"voucherType"`
	CategoryID                        int      `json:"category_id"`
	VMerchantUserID                   string   `json:"merchantUserId"`
	VMerchantCity                     string   `json:"m_city"`
	VMerchantDesc                     string   `json:"m_description"`
	VMerchantCoverPic                 string   `json:"m_cover_pic"`
	VMerchantLogo                     string   `json:"logo_url"`
	VMerchantRating                   float64  `json:"merchant_rating"`
	VCouponInfo                       []string `json:"coupon_info"`
	VPercentOff                       string   `json:"percentOff"`
	VMerchantLocality                 string   `json:"m_locality"`
	VMaxCashback                      float64  `json:"max_cashback"`
	VPercentMax                       float64  `json:"percentage_max"`
	VCashBackPercent                  string   `json:"cashback_percent"`
	MenuPrice                         []string `json:"menuPrice"`
	CouponAmount                      []string `json:"coupon_amount"`
	TncUrl                            string   `json:"tnc_url"`
	VCategory                         string   `json:"category"`
	IsGroupBuyActive                  []bool   `json:"is_groupbuy_active"`
	VOptionID                         string   `json:"option_id"`
	VMerchantID                       int      `json:"merchant_id"`
	VoucherCountry                    string   `json:"voucher_country"`
	IsHidden                          []bool   `json:"is_hidden"`
	IsLocalMerchant                   bool     `json:"is_local_merchant"`
	MaxVoucherUsableBalancePercentMap string   `json:"max_voucher_usable_percent_city_map"`
	VMaxPercentOffCityMap             string   `json:"max_percent_off_without_groupbuy_city_map"`
	VMinCouponAmount                  float64  `json:"min_coupon_amount"`

	//Locality Resolution Field
	LID      int    `json:"id"`
	City     string `json:"city"`
	MegaCity string `json:"mega_city"`
	Cluster  string `json:"cluster"`
	Country  string `json:"country"`
	// Merchant Rating fields
	RatingOne             int      `json:"one_rating"`
	RatingTwo             int      `json:"two_rating"`
	RatingThree           int      `json:"three_rating"`
	RatingFour            int      `json:"four_rating"`
	RatingFive            int      `json:"five_rating"`
	RatingCount           int      `json:"rating_count"`
	IsARQuest             bool     `json:"is_ar_quest"`
	EnableBillPayment     bool     `json:"enable_bill_payment"`
	VoucherMerchantUserID string   `json:"voucher_merchant_user_id"`
	PrimarySubcategory    []string `json:"primary_subcategory"`
}

type Response struct {
	Documents []Document `json:"docs"`
	RawData   []map[string]interface{}
}

type FacetCount struct {
	FacetFields TransactionTags `json:"facet_fields"`
}

type TransactionTags struct {
	TagsWithCount []interface{} `json:"transaction_tags"`
}

type SolrAPIResponse struct {
	Response Response `json:"response"`
}

type SolrAPIFacetResponse struct {
	Response FacetCount `json:"facet_counts"`
}

type solrRawDocs struct {
	Docs []map[string]interface{} `json:"docs"`
}
type solrRawResponse struct {
	Response solrRawDocs `json:"response"`
}

var baseURL string
var baseGeoURL string

func GetReplicaUris(collection string) ([]string, error) {
	s := solr.NewSolrZK(ZKServers, "", collection)

	err := s.Listen()
	if err != nil {
		return nil, err
	}

	loc := s.GetSolrLocator()

	replicas, err := loc.GetReplicaUris()
	if err != nil {
		return nil, err
	}

	uris := make([]string, len(replicas))
	for i, r := range replicas {
		uris[i] = fmt.Sprintf("%s/solr", r)
	}

	return uris, nil
}

func ExecuteGEOQuery(ctx context.Context, localityID int) (res *Response, e error) {
	params := url.Values{}
	params.Add("q", "*:*")
	params.Add("fq", fmt.Sprintf("id:%d", localityID))

	return ExecuteRequest(ctx, SolrGEOCore, params)
}

func ExecuteRequest(ctx context.Context, core string, params url.Values) (*Response, error) {
	if SolrRequestType == "zk" {
		return ExecuteZKRequest(ctx, core, params)
	} else {
		return ExecuteHTTPRequest(ctx, core, params)
	}
}

func ExecuteZKRequest(ctx context.Context, core string, params url.Values) (res *Response, e error) {

	defer func() {
		if err := recover(); err != nil {
			e = fmt.Errorf("%+v", err)
		}
	}()

	must := func(err error) {
		if err != nil {
			panic(err)
		}
	}

	sh, err := solr.NewSolrHTTP(false, core)
	must(err)

	uris, err := GetReplicaUris(core)
	must(err)

	sc := solr.NewSolrHttpRetrier(sh, 1, 2*time.Second)

	var modifiers []func(url.Values)
	for k, p := range params {
		k, p := k, p
		modifiers = append(modifiers, func(v url.Values) {
			v[k] = p
		})
	}

	response, err := sc.Select(uris, modifiers...)
	must(err)

	data, err := json.Marshal(response.Response.Docs)
	must(err)

	var docs []Document
	err = json.Unmarshal(data, &docs)
	must(err)

	res = &Response{
		Documents: docs,
		RawData:   response.Response.Docs,
	}

	return
}

func ExecuteHTTPRequest(ctx context.Context, core string, params url.Values) (*Response, error) {

	var response = new(Response)
	DDAgent := DataDog.Instance()

	var host string
	if strings.EqualFold(core, SolrGEOCore) {
		host = SolrGeoHost
	} else {
		host = SolrHost
	}
	baseURL := fmt.Sprintf("http://%s:%s/solr/%s/", host, SolrPort, core)
	u, err := url.Parse(baseURL)
	if err != nil {
		log.Errorf("Parsing Solr URL Failed: %s %s", core, err)
	}
	u.Path += "select"

	if params == nil {
		params = url.Values{}
	}

	params.Set("wt", "json")
	u.RawQuery = params.Encode()
	req, err := http.NewRequest("GET", u.String(), nil)

	if err != nil {
		log.WithFields(log.Fields{
			"err": err,
		}).Error("HTTP Request Creation Failed")
		DDAgent.Count("solr.request.Error", 1, strings.Fields("HTTP Request Creation"), 1)
		return response, err
	}
	//req.Header.Set("Connection", "close")
	resp, err := solrHTTPClient.Do(req)

	if err != nil {
		log.WithFields(log.Fields{
			"err": err,
		}).Error("Solr HTTP Query Failed")

		DDAgent.Count("solr.request.Error", 1, strings.Fields(core), 1)
		return response, err
	}
	defer resp.Body.Close()

	body, readErr := ioutil.ReadAll(resp.Body)
	if readErr != nil {
		log.WithFields(log.Fields{
			"err":  readErr,
			"body": body,
			"url":  u.String(),
		}).Error("Solr HTTP Read Error")
		DDAgent.Count("solr.request.Error", 1, strings.Fields("HTTP Read Error"), 1)

		return response, err
	}

	var solrAPIResponse SolrAPIResponse
	var rawResponse solrRawResponse
	err = json.Unmarshal(body, &solrAPIResponse)

	if err == nil {
		err = json.Unmarshal(body, &rawResponse)
		if err == nil {
			solrAPIResponse.Response.RawData = rawResponse.Response.Docs
		}
	}

	//err = json.NewDecoder(resp.Body).Decode(&solrAPIResponse)
	if err != nil {
		log.WithFields(log.Fields{
			"err":  err,
			"body": body,
			"url":  u.String(),
		}).Error("Solr Query Failed Unable to parse JSON response")
		DDAgent.Count("solr.request.Error", 1, strings.Fields("JSON Error"), 1)
		return response, err
	}
	DDAgent.Count("solr.request.success", 1, strings.Fields(core), 1)
	return &solrAPIResponse.Response, err
}

func ExecuteHTTPFacetRequest(ctx context.Context, core string, params url.Values) (*FacetCount, error) {

	var response = new(FacetCount)
	DDAgent := DataDog.Instance()

	baseURL := fmt.Sprintf("http://%s:%s/solr/%s/", SolrHost, SolrPort, core)
	u, err := url.Parse(baseURL)
	if err != nil {
		log.Errorf("Parsing Solr URL Failed: %s %s", core, err)
	}
	u.Path += "select"

	if params == nil {
		params = url.Values{}
	}

	params.Set("wt", "json")
	u.RawQuery = params.Encode()
	req, err := http.NewRequest("GET", u.String(), nil)

	if err != nil {
		log.WithFields(log.Fields{
			"err": err,
		}).Error("HTTP Request Creation Failed")
		DDAgent.Count("solr.request.Error", 1, strings.Fields("HTTP Request Creation"), 1)
		return response, err
	}

	ctx, cancel := context.WithTimeout(context.Background(), 500*time.Millisecond)
	resp, err := solrHTTPClient.Do(req.WithContext(ctx))

	if err != nil {
		log.WithFields(log.Fields{
			"err": err,
		}).Error("Solr HTTP Query Failed")
		DDAgent.Count("solr.request.Error", 1, strings.Fields(core), 1)
		return response, err
	}

	if resp != nil {
		cancel()
		defer resp.Body.Close()
	}

	body, readErr := ioutil.ReadAll(resp.Body)
	if readErr != nil {
		log.WithFields(log.Fields{
			"err":  readErr,
			"body": body,
			"url":  u.String(),
		}).Error("Solr HTTP Read Error")
		DDAgent.Count("solr.request.Error", 1, strings.Fields("HTTP Read Error"), 1)
		return response, err
	}

	var facetResponse SolrAPIFacetResponse
	err = json.Unmarshal(body, &facetResponse)
	//err = json.NewDecoder(resp.Body).Decode(&solrAPIResponse)
	if err != nil {
		log.WithFields(log.Fields{
			"err":  err,
			"body": body,
			"url":  u.String(),
		}).Error("Solr Query Failed Unable to parse JSON response")
		DDAgent.Count("solr.request.Error", 1, strings.Fields("JSON Error"), 1)
		return response, err
	}
	DDAgent.Count("solr.request.success", 1, strings.Fields(core), 1)
	return &facetResponse.Response, err
}

//Resolve Locality Query on Solr
/*
http://g2.gc.magicpin.in:8983/solr/geo/select?
q=*:*&
wt=json&
indent=true&
fq={!bbox}&
sfield=latlon&
pt=28.4721754,77.0717681&
d=10&
sort=geodist()%20asc&
fl=locality,id,cluster,city,mega_city
*/
func ExecuteResolveLatLongQuery(num int, latlon string) (*Response, error) {
	var ctx context.Context

	rows := strconv.Itoa(num)
	//Add parameters
	parameters := url.Values{}
	parameters.Add("q", "*:*")
	parameters.Add("wt", "json")
	parameters.Add("indent", "true")
	parameters.Add("rows", rows)
	parameters.Add("d", "10")
	parameters.Add("fq", "{!bbox}")
	parameters.Add("sfield", "latlon")
	parameters.Add("pt", latlon)
	parameters.Add("sort", "geodist() asc")
	parameters.Add("fl", "locality,id,cluster,city,mega_city")

	return ExecuteRequest(ctx, SolrGEOCore, parameters)
}

//Distance Query on Solr
/*
http://g2.gc.magicpin.in:8983/solr/deals/select?
q=search_text:*&
wt=json&
fq=deal_visibility:VISIBLE&
fq={!bbox}&
sfield=latlon&
pt=28.6315,77.2167&
d=10&
indent=true&
fq=custom_tags:MadeInChina
*/
func ExecuteLocalityCenteredQuery(ctx context.Context, num int, latlon string, customTag string) (*Response, error) {

	rows := strconv.Itoa(num)
	query := "(custom_tags:\"%s\" OR tags:\"%s\")"
	query = fmt.Sprintf(query, customTag, customTag)

	parameters := url.Values{}
	parameters.Add("q", "search_text:*")
	parameters.Add("wt", "json")
	parameters.Add("fq", "deal_visibility:VISIBLE")
	parameters.Add("fq", "is_campaign:false")
	parameters.Add("indent", "true")
	parameters.Add("fq", "rating:[4 TO *]")
	parameters.Add("rows", rows)
	parameters.Add("d", rows)
	parameters.Add("fq", "{!bbox}")
	parameters.Add("sfield", "latlon")
	parameters.Add("pt", latlon)
	parameters.Add("fq", query)

	return ExecuteRequest(ctx, SolrCore, parameters)
}

func ExecuteQuery(ctx context.Context, num int, locality_id int, customTag string) (*Response, error) {
	rows := strconv.Itoa(num)
	locality := "locality_id:" + strconv.Itoa(locality_id)
	query := "(custom_tags:\"%s\" OR tags:\"%s\")"
	query = fmt.Sprintf(query, customTag, customTag)

	parameters := url.Values{}
	parameters.Add("q", "search_text:*")
	parameters.Add("wt", "json")
	parameters.Add("fq", "deal_visibility:VISIBLE")
	parameters.Add("fq", "is_campaign:false")
	parameters.Add("indent", "true")
	parameters.Add("fq", "rating:[4 TO *]")
	parameters.Add("rows", rows)
	parameters.Add("fq", query)
	parameters.Add("fq", locality)

	return ExecuteRequest(ctx, SolrCore, parameters)

}

func ExecuteQueryNoLocality(num int, customTag string) (*Response, error) {
	var ctx context.Context
	rows := strconv.Itoa(num)

	query := "(custom_tags:\"%s\" OR tags:\"%s\")"
	query = fmt.Sprintf(query, customTag, customTag)

	parameters := url.Values{}
	parameters.Add("q", "search_text:*")
	parameters.Add("wt", "json")
	parameters.Add("fq", "deal_visibility:VISIBLE")
	parameters.Add("fq", "is_campaign:false")
	parameters.Add("indent", "true")
	parameters.Add("fq", "rating:[4 TO *]")
	parameters.Add("rows", rows)
	parameters.Add("fq", query)

	return ExecuteRequest(ctx, SolrCore, parameters)
}

func ExecuteSolrQuery(ctx context.Context, latLon string, filters []string, num int) (*Response, error) {
	span, ctx := tracer.NewChildSpanWithContext("ExecuteSolrQuery", ctx)
	defer span.Finish()

	rows := strconv.Itoa(num)
	parameters := url.Values{}
	parameters.Add("q", "search_text:*")
	parameters.Add("indent", "true")
	parameters.Add("wt", "json")
	parameters.Add("rows", rows)
	parameters.Add("pt", latLon)

	for _, f := range filters {
		splitter := strings.Split(f, "&")
		for _, param := range splitter {
			s := strings.Split(param, "=")
			if len(s) == 2 {
				key, value := s[0], s[1]
				parameters.Add(key, value)
			}
		}
	}

	return ExecuteRequest(ctx, SolrCore, parameters)
}

func ExecuteVoucherUrl(ctx context.Context, filters []string, num int) (*Response, error) {
	span, ctx := tracer.NewChildSpanWithContext("ExecuteVoucherSolrQuery", ctx)
	defer span.Finish()

	rows := strconv.Itoa(num)
	parameters := url.Values{}
	parameters.Add("q", "*:*")
	parameters.Add("indent", "true")
	parameters.Add("wt", "json")
	parameters.Add("rows", rows)

	for _, f := range filters {
		splitter := strings.Split(f, "&")
		for _, param := range splitter {
			s := strings.Split(param, "=")
			if len(s) == 2 {
				key, value := s[0], s[1]
				parameters.Add(key, value)
			}
		}
	}

	return ExecuteRequest(ctx, SolrVoucherCore, parameters)
}

func ExecuteMerchantUrl(ctx context.Context, query string, num int) (*Response, error) {
	span, ctx := tracer.NewChildSpanWithContext("ExecuteMerchantSolrQuery", ctx)
	defer span.Finish()

	rows := strconv.Itoa(num)
	parameters := url.Values{}
	parameters.Add("q", query)
	parameters.Add("indent", "true")
	parameters.Add("wt", "json")
	parameters.Add("rows", rows)

	return ExecuteRequest(ctx, SolrMerchantCore, parameters)
}

//http://g2.gc.magicpin.in:8983/solr/deals/
//select?q=search_text:*&wt=json&f
//q=deal_visibility:VISIBLE&fq=is_campaign:false&indent=true&rows=10&
//fq=custom_tags:HeavenlyBurgers&fq=locality_id:11&fq=rating:[4%20TO%20*]

//GEO Query on Solr
/*
http://g2.gc.magicpin.in:8983/solr/geo/select?
q=*:*&
wt=json&
indent=true&
fq=id:11
*/

var (
	SolrHost         = os.Getenv("SOLR_HOST")
	SolrGeoHost      = os.Getenv("SOLR_GEO_HOST")
	SolrCore         = os.Getenv("SOLR_CORE")
	SolrPort         = os.Getenv("SOLR_PORT")
	ZKServers        = os.Getenv("ZOOKEEPER_SERVERS")
	SolrRequestType  = os.Getenv("SOLR_REQUEST_TYPE")
	SolrGEOCore      = "geo" //os.Getenv("SOLR_GEO_CORE")
	SolrVoucherCore  = "voucher"
	SolrMerchantCore = "merchant"
	SolrDealsCore    = "deals"
	SolrTxfeedCore   = "txfeed"
	solrHTTPClient   = HttpClient.Instance().Client
)

func init() {
	baseURL = "http://%s:%s/solr/%s/"
	baseURL = fmt.Sprintf(baseURL, SolrHost, SolrPort, SolrCore)
	baseGeoURL = "http://%s:%s/solr/%s/select?q=*:*&wt=json&indent=true"
	baseGeoURL = fmt.Sprintf(baseGeoURL, SolrHost, SolrPort, SolrGEOCore)

	if SolrRequestType == "" {
		SolrRequestType = "zk"
	}

	//baseMerchantDealURL += `&fq=product_name:"All Store"`
	//baseURL = url.QueryEscape(baseURL)
	log.WithFields(log.Fields{
		"Solr Host":     SolrHost,
		"Solr Core":     SolrCore,
		"Solr Port":     SolrPort,
		"Solr GEO Core": SolrGEOCore,
		"Base URL":      baseURL,
		"Base GEO URL":  baseGeoURL,
	}).Info("Solr Connection Parameters")
}
func ExecuteMerchantQueryWithDealDesc(ctx context.Context, merchantID int) (*Response, error) {
	mid := strconv.Itoa(merchantID)

	parameters := url.Values{}
	parameters.Add("q", "merchant_id:"+mid)
	parameters.Add("fq", "-deal_category:\"brand\"")
	parameters.Add("fq", "deal_desc:\"All Store\"")
	parameters.Add("wt", "json")

	return ExecuteRequest(ctx, SolrDealsCore, parameters)
}

func ExecuteMerchantQueryWithVoucher(ctx context.Context, merchantUserID int, city string) (*Response, error) {

	muid := strconv.Itoa(merchantUserID)

	parameters := url.Values{}
	parameters.Add("q", "voucher_merchant_user_id:"+muid)
	parameters.Add("wt", "json")
	parameters.Add("rows", "1")

	return ExecuteRequest(ctx, SolrVoucherCore, parameters)
}

func GetRecommendedMerchants(ctx context.Context, latlon string, limit int) (*Response, error) {
	span, ctx := tracer.NewChildSpanWithContext("SolrQueryPrimarySubCategory", ctx)
	defer span.Finish()
	rows := strconv.Itoa(limit)

	parameters := url.Values{}
	parameters.Add("q", "search_text:*")
	parameters.Add("wt", "json")
	parameters.Add("fq", "deal_visibility:VISIBLE")
	parameters.Add("fq", "is_campaign:false")
	parameters.Add("indent", "true")
	parameters.Add("fq", "rating:[4 TO *]")
	parameters.Add("rows", rows)
	parameters.Add("d", rows)
	parameters.Add("fq", "{!bbox}")
	parameters.Add("sfield", "latlon")
	parameters.Add("pt", latlon)
	parameters.Add("sort", "geodist() asc")
	parameters.Add("fl", "category_id,country,cost_for_two,cashback_percent,cashback_value,latlon,cdn_url,merchant_desc,city,merchant_logo,locality_id,locality,rating,merchant_cover_pic,merchant_id,merchant_name,user_id,deal_id,primary_subcategory")
	return ExecuteRequest(ctx, SolrCore, parameters)
}

func ExecuteVoucherQueryWithROid(ctx context.Context, voucherID int) (*Response, error) {

	roid := strconv.Itoa(voucherID)
	parameters := url.Values{}
	parameters.Add("q", "*:*")
	parameters.Add("fq", "redemption_option_amount_id:("+roid+")")
	parameters.Add("indent", "true")
	parameters.Add("wt", "json")
	parameters.Add("rows", "1")

	return ExecuteRequest(ctx, SolrVoucherCore, parameters)
}

func ExecuteMerchantRatingFetchQueryWithMerchantID(ctx context.Context, merchantID int) (*Response, error) {
	mid := strconv.Itoa(merchantID)
	parameters := url.Values{}
	parameters.Add("q", "*:*")
	parameters.Add("fq", "merchant_id:"+mid)
	parameters.Add("wt", "json")
	parameters.Add("indent", "true")

	return ExecuteRequest(ctx, SolrMerchantCore, parameters)
}

func GetDealFromMerchants(ctx context.Context, merchantIDs []int) (*Response, error) {
	ids := strings.Trim(strings.Replace(fmt.Sprint(merchantIDs), " ", " ", -1), "[]")
	parameters := url.Values{}
	parameters.Add("q", "merchant_id: ("+ids+")")
	parameters.Add("fq", "deal_visibility:VISIBLE")
	parameters.Add("wt", "json")
	return ExecuteRequest(ctx, SolrDealsCore, parameters)
}

func GetVoucherFromMerchants(ctx context.Context, merchantIDs []int) (*Response, error) {
	ids := strings.Trim(strings.Replace(fmt.Sprint(merchantIDs), " ", " ", -1), "[]")
	parameters := url.Values{}
	parameters.Add("q", "*:*")
	parameters.Add("fq", "voucher_merchant_user_id: ("+ids+")")
	parameters.Add("fl", "redemption_option_id,voucher_merchant_user_id")
	parameters.Add("wt", "json")
	return ExecuteRequest(ctx, SolrVoucherCore, parameters)
}

func ExecuteUserTags(ctx context.Context, uID int) (*FacetCount, error) {
	span, ctx := tracer.NewChildSpanWithContext("ExecuteMerchantRatingFetchQueryWithMerchantID", ctx)
	defer span.Finish()
	mid := strconv.Itoa(uID)
	parameters := url.Values{}
	parameters.Add("q", "*:*")
	parameters.Add("fq", "customer_user_id:"+mid)
	parameters.Add("wt", "json")
	parameters.Add("indent", "true")
	parameters.Add("facet", "true")
	parameters.Add("facet.mincount", "1")
	parameters.Add("facet.field", "transaction_tags")
	parameters.Add("facet.mincount", "1")
	parameters.Add("rows", "0")
	return ExecuteHTTPFacetRequest(ctx, SolrTxfeedCore, parameters)
}

//http://g2.gc.magicpin.in:8983/solr/deals/select?q=user_id:345426&fq=deal_visibility:VISIBLE&fl=max_cashback&wt=json&rows=1
func GetMaxCashbackDealFromMerchant(ctx context.Context, merchantUserId int) (*Response, error) {
	parameters := url.Values{}
	parameters.Add("q", fmt.Sprintf("user_id:%d", merchantUserId))
	parameters.Add("fq", "deal_visibility:VISIBLE")
	parameters.Add("fl", "max_cashback,percentage_max")
	parameters.Add("wt", "json")
	parameters.Add("rows", "1")
	return ExecuteRequest(ctx, SolrDealsCore, parameters)
}
