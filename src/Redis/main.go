package Redis

import (
	"fmt"
	"../DataDog"
	"os"
	"strconv"
	"strings"
	"sync"
	"time"

	"github.com/go-redis/redis"
	log "github.com/sirupsen/logrus"
)

var (
	redis_max_connection      = os.Getenv("REDIS_MAX_CONNECTION")
	redis_max_wait_ms         = os.Getenv("REDIS_MAX_WAIT_MS")
	redis_password            = os.Getenv("REDIS_PASSWORD")
	redis_addr                = os.Getenv("REDIS_ADDR")
	cache_addr                = os.Getenv("REDIS_CACHE_ADDR")
	trending_cache_addr       = os.Getenv("TRENDING_CACHE_ADDR")
	notification_cache_addr   = os.Getenv("NOTIFICATION_CACHE_ADDR")
	notification_cache_passwd = os.Getenv("NOTIFICATION_CACHE_PASSWORD")
	RedisClient               *redis.Client
	CacheClient               *redis.Client
	MRedisClient              *redis.Client
	NotificationRedisClient   *redis.Client
	expiryTime                = time.Minute
	SleepDuration             = time.Second
	Nil                       = redis.Nil
	redisGenosCacheOnce       sync.Once
	redisNotificationOnce     sync.Once
	redisMRedisOnce           sync.Once
	redisGenosTrendingOnce    sync.Once
)

func init() {
	initCache()
	initNotificationCache()
	initMRedis()
	redisOption := redis.Options{}
	//log.Info("redis_max_connection is ", os.Getenv("REDIS_MAX_CONNECTION"))
	pInt, pErr := strconv.Atoi(os.Getenv("REDIS_MAX_CONNECTION"))
	if pErr != nil {
		log.Error("error while converting redis max connection to int : ", pErr)
		panic(pErr)
	}

	redisOption.PoolSize = pInt

	pInt64, pErr64 := strconv.ParseInt(os.Getenv("REDIS_MAX_WAIT_MS"), 10, 64)
	if pErr64 != nil {
		log.Error("error while converting redis max wait ms to log : ", pErr64)
		panic(pErr64)
	}

	log.Info("trending redis address is ", trending_cache_addr)
	log.Info("trending redis password is ", "")
	redisOption.PoolTimeout = time.Duration(pInt64)
	redisOption.Addr = trending_cache_addr
	redisOption.ReadTimeout = 200 * time.Millisecond
	redisOption.Password = ""
	redisGenosTrendingOnce.Do(func() {
		RedisClient = redis.NewClient(&redisOption)
	})

	pong, err := RedisClient.Ping().Result()
	fmt.Println(pong, err)
}

func initCache() {
	redisOption := redis.Options{}
	//log.Info("redis_max_connection is ", os.Getenv("REDIS_MAX_CONNECTION"))
	pInt, pErr := strconv.Atoi(os.Getenv("REDIS_MAX_CONNECTION"))
	if pErr != nil {
		log.Error("error while converting redis max connection to int : ", pErr)
		panic(pErr)
	}

	redisOption.PoolSize = pInt

	pInt64, pErr64 := strconv.ParseInt(os.Getenv("REDIS_MAX_WAIT_MS"), 10, 64)
	if pErr64 != nil {
		log.Error("error while converting redis max wait ms to log : ", pErr64)
		panic(pErr64)
	}

	log.Info("redis cache address is ", cache_addr)
	log.Info("redis cache password is ", redis_password)
	redisOption.PoolTimeout = time.Duration(pInt64)
	redisOption.Addr = cache_addr
	redisOption.Password = redis_password
	redisGenosCacheOnce.Do(func() {
		CacheClient = redis.NewClient(&redisOption)
	})
	pong, err := CacheClient.Ping().Result()
	fmt.Println(pong, err)
}

func initMRedis() {
	redisOption := redis.Options{}
	//log.Info("redis_max_connection is ", os.Getenv("REDIS_MAX_CONNECTION"))
	pInt, pErr := strconv.Atoi(os.Getenv("REDIS_MAX_CONNECTION"))
	if pErr != nil {
		log.Error("error while converting redis max connection to int : ", pErr)
		panic(pErr)
	}

	redisOption.PoolSize = pInt

	pInt64, pErr64 := strconv.ParseInt(os.Getenv("REDIS_MAX_WAIT_MS"), 10, 64)
	if pErr64 != nil {
		log.Error("error while converting redis max wait ms to log : ", pErr64)
		panic(pErr64)
	}

	log.Info("m-redis cache address is ", redis_addr)
	log.Info("m-redis cache password is ", redis_password)
	redisOption.PoolTimeout = time.Duration(pInt64)
	redisOption.Addr = redis_addr
	redisOption.Password = redis_password

	redisMRedisOnce.Do(func() {
		MRedisClient = redis.NewClient(&redisOption)
	})
	pong, err := MRedisClient.Ping().Result()
	fmt.Println(pong, err)
}

func initNotificationCache() {
	redisOption := redis.Options{}
	//log.Info("redis_max_connection is ", os.Getenv("REDIS_MAX_CONNECTION"))
	pInt, pErr := strconv.Atoi(os.Getenv("REDIS_MAX_CONNECTION"))
	if pErr != nil {
		log.Error("error while converting redis max connection to int : ", pErr)
		panic(pErr)
	}

	redisOption.PoolSize = pInt

	pInt64, pErr64 := strconv.ParseInt(os.Getenv("REDIS_MAX_WAIT_MS"), 10, 64)
	if pErr64 != nil {
		log.Error("error while converting redis max wait ms to log : ", pErr64)
		panic(pErr64)
	}

	log.Info("redis cache address is ", notification_cache_addr)
	log.Info("redis cache password is ", notification_cache_passwd)
	redisOption.PoolTimeout = time.Duration(pInt64)
	redisOption.Addr = notification_cache_addr
	redisOption.Password = notification_cache_passwd

	redisNotificationOnce.Do(func() {
		NotificationRedisClient = redis.NewClient(&redisOption)
	})
	pong, err := NotificationRedisClient.Ping().Result()
	fmt.Println(pong, err)
}

func GetInstance() *redis.Client {
	return RedisClient
}

func CheckKeyExistance(key string) bool {
	log.Debug("Checking for key ", key)
	datadog := DataDog.Instance()
	val, err := RedisClient.Get(key).Result()
	datadog.Count("consumer.redis.read.success", 1, strings.Fields("lock_check"), 1)
	if err == redis.Nil {
		return false
	} else if err != nil {
		log.WithFields(log.Fields{
			"error": err,
		}).Error("while checking lock existence")
		return false

	} else {
		if val == "1" {
			return true
		} else {
			return false
		}
	}
}

func AcquireLock(key string) bool {
	log.Debug("Getting a Lock")
	datadog := DataDog.Instance()
	result, err := RedisClient.SetNX(key, "1", expiryTime).Result()
	if err != nil {
		log.WithFields(log.Fields{
			"error": err,
		}).Error("while acquiring redis lock")
		datadog.Count("consumer.redis.write.failure", 1, strings.Fields("acquire_lock"), 1)
		return false
	}
	datadog.Count("consumer.redis.write.success", 1, strings.Fields("acquire_lock"), 1)
	return result
}

func ReleaseLock(key string) bool {
	datadog := DataDog.Instance()
	val, err := RedisClient.Del(key).Result()
	if err != nil {
		log.WithFields(log.Fields{
			"Error": err,
		}).Error("while releasing redis lock")
		datadog.Count("consumer.redis.delete.failure", 1, strings.Fields("lock"), 1)
	} else if val == 1 {
		datadog.Count("consumer.redis.delete.success", 1, strings.Fields("lock"), 1)
		return true
	}
	return false
}

func ExecuteQueryLock(key string) bool {
	var (
		cnt = 0
	)
	// Loop till key does not exists
	for cnt < 5 && CheckKeyExistance(key) {
		//sleep for 1 sec
		time.Sleep(SleepDuration)
		cnt++
		log.Debug("Sleeping for 1s waiting for acquiring lock for key = ", key)
	}
	if cnt < 5 {
		AcquireLock(key)
	}
	return true
	//defer func() {
	//	// Keep in mind lock auto expires in 1 minute
	//	if success && key != "" {
	//		log.Debug("Releasing lock for ", key, " success = ", releaseLock(key))
	//	}
	//}()
}
