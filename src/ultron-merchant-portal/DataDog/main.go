package DataDog

import (
	"github.com/DataDog/datadog-go/statsd"
	log "github.com/sirupsen/logrus"
	"sync"
)

var (
	dd   *statsd.Client
	once sync.Once
)

func Instance() *statsd.Client {
	once.Do(func() {
		DDAgent, err := statsd.New("127.0.0.1:8125")
		if err != nil {
			log.Error(err)
		}
		log.WithFields(log.Fields{
			"statsd Host": "127.0.0.1:8125",
		}).Info("Connecting to statsd ")
		DDAgent.SimpleEvent("in.magicpin.uig.api.initialize", "success")
		DDAgent.Namespace = "in.magicpin.uig.api."
		dd = DDAgent
	})
	return dd
}
