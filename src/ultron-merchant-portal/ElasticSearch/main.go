package ElasticSearch

import (
	"bytes"
	"context"
	"fmt"
	log "github.com/sirupsen/logrus"
	"gopkg.in/olivere/elastic.v5"
	"io/ioutil"
	"net/http"
	"os"
	"sync"
)

var (
	esHost          = os.Getenv("ES_HOST")
	esAnalyticsHost = os.Getenv("ES_ANALYTICS_HOST")
	es              *ElasticSearch
	esAnalytics     *ElasticSearch
	once            sync.Once
	analyticsOnce   sync.Once
)

type ElasticSearch struct {
	Client      *elastic.Client
	IsConnected bool
}

type Settings struct {
	Shards   int `json:"number_of_shards"`
	Replicas int `json:"number_of_replicas"`
}

type Mapping struct {
	Settings Settings    `json:"settings"`
	Mappings interface{} `json:"mappings,omitempty"`
}

func NewAnalyticsClient() *ElasticSearch {
	esAnalytics := ElasticSearch{}

	client, err := elastic.NewClient(elastic.SetURL(esAnalyticsHost))
	if err != nil {
		log.Errorf("Elastic Search Analytics connection to host %s failed: %s", esAnalyticsHost, err)
		esAnalytics.IsConnected = false
	} else {
		log.Infof("Connected to ElasticSearch Analytics Cluster : %s", esAnalyticsHost)
		esAnalytics.IsConnected = true
	}
	esAnalytics.Client = client
	return &esAnalytics
}

func NewClient() *ElasticSearch {
	es := ElasticSearch{}

	client, err := elastic.NewClient(elastic.SetURL(esHost))
	if err != nil {
		log.Errorf("Elastic Search connection to host %s failed: %s", esHost, err)
		es.IsConnected = false
	} else {
		log.Infof("Connected to ElasticSearch Cluster : %s", esHost)
		es.IsConnected = true
	}
	es.Client = client
	return &es
}

func Instance() *ElasticSearch {
	once.Do(func() {
		es = NewClient()
	})
	return es
}

func AnalyticsInstance() *ElasticSearch {
	log.Infof("Creating ES Analytics instance")
	analyticsOnce.Do(func() {
		esAnalytics = NewAnalyticsClient()
	})
	return esAnalytics
}

func (es *ElasticSearch) CreateIndex(name string, shards int, replicas int) (*elastic.IndicesCreateResult, bool) {
	var err error
	var index *elastic.IndicesCreateResult
	ctx := context.Background()

	mapping := Mapping{
		Settings: Settings{Shards: shards, Replicas: replicas},
		Mappings: nil}

	index, err = es.Client.CreateIndex(name).BodyJson(mapping).Do(ctx)

	if err != nil {
		log.Errorf("creating '%s' index failed: %s", name, err)
		return index, false
	}

	if !index.Acknowledged {
		log.Errorf("creating '%s' index is not acknowledged.", name)
	}
	return index, true
}

func (es *ElasticSearch) Search(index string, Type string, query string) ([]byte, error) {
	url := fmt.Sprintf("%s/%s/%s/_search", esHost, index, Type)
	var jsonStr = []byte(query)
	req, err := http.NewRequest("POST", url, bytes.NewBuffer(jsonStr))
	req.Header.Set("Content-Type", "application/json")

	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		log.Errorf("ES Search Error: %s %s", url, err)
		return []byte{}, err
	}
	defer resp.Body.Close()
	body, readErr := ioutil.ReadAll(resp.Body)
	if readErr != nil {
		log.WithFields(log.Fields{
			"err":  readErr,
			"body": body,
			"url":  url,
		}).Error("ES HTTP Read Error")
		return []byte{}, err
	}

	log.Debug("ES Query '%s' executed successfully", url)
	//log.Infof("Result: %s", string(body))
	return body, nil
}
