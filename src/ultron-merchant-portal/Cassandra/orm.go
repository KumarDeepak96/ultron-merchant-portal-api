package Cassandra

import (
	"fmt"
	"github.com/gocql/gocql"
	log "github.com/sirupsen/logrus"
	"gopkg.in/inf.v0"
	"reflect"
	"strconv"
	"strings"
	"sync"
	"time"
)

type CQLORM struct{}

func (o *CQLORM) Update(model CQLModel) (*gocql.Query, error) {
	return PartialUpdate(model)
}

func (o *CQLORM) Get(model CQLModel) (err error) {
	defer func() {
		if e := recover(); e != nil {
			log.Errorf("Panic happened in Cassandra.PartialUpdate: %+v", e)
			err = CQLError{message: fmt.Sprintf("panic: %+v", e)}
		}
	}()

	selectQuery := "select * from " + model.TableName() + " where "

	vals := reflect.ValueOf(model)
	var whereClause []string
	var fieldValues []interface{}
	if vals.Type().Kind() == reflect.Ptr {
		vals = vals.Elem()
	}

	for i := 0; i < vals.NumField(); i++ {
		tag := strings.Split(vals.Type().Field(i).Tag.Get("cql"), ",")
		if len(tag) > 2 && tag[2] == "primarykey" {

			if !isFieldValid(vals.Field(i)) {
				return CQLError{message: "Primary Key cannot be nil"}
			}
			var fieldValue interface{}
			if vals.Field(i).Type().Kind() == reflect.Ptr {
				fieldValue = vals.Field(i).Elem().Interface()
			} else {
				fieldValue = vals.Field(i).Interface()

			}
			fieldValues = append(fieldValues, fieldValue)
			whereClause = append(whereClause, fmt.Sprintf("%s=?", tag[0]))
		}

	}
	selectQuery = selectQuery + strings.Join(whereClause, " and ")
	iter := cql.Session.Query(selectQuery, fieldValues...).Iter()

	row := map[string]interface{}{}

	for iter.MapScan(row) {
		var wg sync.WaitGroup
		vals := reflect.ValueOf(model)
		if vals.Kind() == reflect.Ptr {
			vals = vals.Elem()
		}
		for i := 0; i < vals.NumField(); i++ {
			rt := vals.Field(i).Type().Kind()

			isPointer := rt == reflect.Ptr
			if isPointer {
				rt = vals.Field(i).Type().Elem().Kind()
			}

			tag := strings.Split(vals.Type().Field(i).Tag.Get("cql"), ",")

			if len(tag) == 0 || (len(tag) == 1 && tag[0] == "") {
				continue
			}
			colName := tag[0]
			var colType string
			if len(tag) == 2 {
				colType = tag[1]
			}

			if vals.Field(i).Type().Kind() == reflect.Ptr {
				vals.Field(i).Set(reflect.New(vals.Field(i).Type().Elem()))
			}
			if vals.Field(i).CanSet() && vals.Field(i).Type().Kind() == reflect.Ptr &&
				vals.Field(i).Elem().Type().Kind() == reflect.Struct && vals.Field(i).Elem().Type().Implements(reflect.TypeOf((*CQLModel)(nil)).Elem()) {
				var err CQLError
				wg.Add(1)
				go func(vals reflect.Value, i int) {
					defer wg.Done()
					tag := strings.Split(vals.Type().Field(i).Tag.Get("cql"), ",")

					if len(tag) < 4 {
						err = CQLError{message: "Primary Key cannot be nil"}
						return
					}
					primaryKeyForNestedStruct := tag[3]
					nestedVals := reflect.ValueOf(vals.Field(i).Interface().(CQLModel))

					if nestedVals.Type().Kind() == reflect.Ptr {
						nestedVals = nestedVals.Elem()
					}

					for i := 0; i < nestedVals.NumField(); i++ {
						tag := strings.Split(nestedVals.Type().Field(i).Tag.Get("cql"), ",")
						if len(tag) > 2 && tag[2] == "primarykey" {

							if nestedVals.Field(i).Type().Kind() == reflect.Ptr {
								nestedVals.Field(i).Set(reflect.New(nestedVals.Field(i).Type().Elem()))
								nestedVals.Field(i).Elem().Set(reflect.ValueOf(row[primaryKeyForNestedStruct]))
							} else {
								nestedVals.Field(i).Set(reflect.ValueOf(row[primaryKeyForNestedStruct]))

							}
						}

					}

					cql.ORM.Get(vals.Field(i).Interface().(CQLModel))
				}(vals, i)
				if err.message != "" {
					return err
				}

			} else if vals.Field(i).CanSet() && colName != "" {

				switch row[colName].(type) {
				case int:
					assign := row[colName].(int)
					vals.Field(i).Set(reflect.New(vals.Field(i).Type().Elem()))
					vals.Field(i).Elem().SetInt(int64(assign))
				case string:
					assign := row[colName].(string)
					vals.Field(i).Set(reflect.New(vals.Field(i).Type().Elem()))
					vals.Field(i).Elem().SetString(assign)
				case []string:
					assign := row[colName].([]string)
					vals.Field(i).Set(reflect.ValueOf(assign))
				case []int:
					assign := row[colName].([]int)
					vals.Field(i).Set(reflect.ValueOf(assign))
				case *inf.Dec:
					if assign, err := strconv.ParseFloat(row[colName].(*inf.Dec).String(), 64); err == nil {
						vals.Field(i).Elem().Set(reflect.ValueOf(assign))
					}
				case time.Time:
					assign := row[colName].(time.Time)
					if vals.Field(i).Type().Kind() == reflect.String {
						if colType == "date" {
							vals.Field(i).SetString(assign.Format("02-01-2006"))
						}
					} else if !assign.IsZero() {
						vals.Field(i).Elem().Set(reflect.ValueOf(assign.Unix()))
					}
				case bool:
					assign := row[colName].(bool)
					vals.Field(i).Elem().SetBool(assign)
				}
			}
		}
		wg.Wait()
		break
	}

	return nil
}

func (o *CQLORM) GetNonPrivatePost(model CQLModel) (err error) {
	defer func() {
		if e := recover(); e != nil {
			log.Errorf("Panic happened in Cassandra.PartialUpdate: %+v", e)
			err = CQLError{message: fmt.Sprintf("panic: %+v", e)}
		}
	}()

	selectQuery := "select * from " + model.TableName() + " where "

	vals := reflect.ValueOf(model)
	var whereClause []string
	var fieldValues []interface{}
	if vals.Type().Kind() == reflect.Ptr {
		vals = vals.Elem()
	}

	for i := 0; i < vals.NumField(); i++ {
		tag := strings.Split(vals.Type().Field(i).Tag.Get("cql"), ",")
		if len(tag) > 2 && tag[2] == "primarykey" {

			if !isFieldValid(vals.Field(i)) {
				return CQLError{message: "Primary Key cannot be nil"}
			}
			var fieldValue interface{}
			if vals.Field(i).Type().Kind() == reflect.Ptr {
				fieldValue = vals.Field(i).Elem().Interface()
			} else {
				fieldValue = vals.Field(i).Interface()

			}
			fieldValues = append(fieldValues, fieldValue)
			whereClause = append(whereClause, fmt.Sprintf("%s=?", tag[0]))
		}

	}
	selectQuery = selectQuery + strings.Join(whereClause, " and ")
	iter := cql.Session.Query(selectQuery, fieldValues...).Iter()

	row := map[string]interface{}{}

	for iter.MapScan(row) {
		var wg sync.WaitGroup
		vals := reflect.ValueOf(model)
		if vals.Kind() == reflect.Ptr {
			vals = vals.Elem()
		}
		for i := 0; i < vals.NumField(); i++ {
			rt := vals.Field(i).Type().Kind()

			isPointer := rt == reflect.Ptr
			if isPointer {
				rt = vals.Field(i).Type().Elem().Kind()
			}

			tag := strings.Split(vals.Type().Field(i).Tag.Get("cql"), ",")

			if len(tag) == 0 || (len(tag) == 1 && tag[0] == "") {
				continue
			}
			colName := tag[0]
			var colType string
			if len(tag) == 2 {
				colType = tag[1]
			}

			if vals.Field(i).Type().Kind() == reflect.Ptr {
				vals.Field(i).Set(reflect.New(vals.Field(i).Type().Elem()))
			}
			if vals.Field(i).CanSet() && vals.Field(i).Type().Kind() == reflect.Ptr &&
				vals.Field(i).Elem().Type().Kind() == reflect.Struct && vals.Field(i).Elem().Type().Implements(reflect.TypeOf((*CQLModel)(nil)).Elem()) {
				var err CQLError
				wg.Add(1)
				go func(vals reflect.Value, i int) {
					defer wg.Done()
					//Not calling recursive functions for private posts
					if row["visibility"] != nil && strings.EqualFold(row["visibility"].(string), "self") {
						return
					}

					tag := strings.Split(vals.Type().Field(i).Tag.Get("cql"), ",")

					if len(tag) < 4 {
						err = CQLError{message: "Primary Key cannot be nil"}
					}
					primaryKeyForNestedStruct := tag[3]
					nestedVals := reflect.ValueOf(vals.Field(i).Interface().(CQLModel))

					if nestedVals.Type().Kind() == reflect.Ptr {
						nestedVals = nestedVals.Elem()
					}

					for i := 0; i < nestedVals.NumField(); i++ {
						tag := strings.Split(nestedVals.Type().Field(i).Tag.Get("cql"), ",")
						if len(tag) > 2 && tag[2] == "primarykey" {

							if nestedVals.Field(i).Type().Kind() == reflect.Ptr {
								nestedVals.Field(i).Set(reflect.New(nestedVals.Field(i).Type().Elem()))
								nestedVals.Field(i).Elem().Set(reflect.ValueOf(row[primaryKeyForNestedStruct]))
							} else {
								nestedVals.Field(i).Set(reflect.ValueOf(row[primaryKeyForNestedStruct]))

							}
						}

					}

					cql.ORM.Get(vals.Field(i).Interface().(CQLModel))
				}(vals, i)
				if err.message != "" {
					return err
				}

			} else if vals.Field(i).CanSet() && colName != "" {

				switch row[colName].(type) {
				case int:
					assign := row[colName].(int)
					vals.Field(i).Set(reflect.New(vals.Field(i).Type().Elem()))
					vals.Field(i).Elem().SetInt(int64(assign))
				case string:
					assign := row[colName].(string)
					vals.Field(i).Set(reflect.New(vals.Field(i).Type().Elem()))
					vals.Field(i).Elem().SetString(assign)
				case []string:
					assign := row[colName].([]string)
					vals.Field(i).Set(reflect.ValueOf(assign))
				case []int:
					assign := row[colName].([]int)
					vals.Field(i).Set(reflect.ValueOf(assign))
				case *inf.Dec:
					if assign, err := strconv.ParseFloat(row[colName].(*inf.Dec).String(), 64); err == nil {
						vals.Field(i).Elem().Set(reflect.ValueOf(assign))
					}
				case time.Time:
					assign := row[colName].(time.Time)
					if vals.Field(i).Type().Kind() == reflect.String {
						if colType == "date" {
							vals.Field(i).SetString(assign.Format("02-01-2006"))
						}
					} else if !assign.IsZero() {
						vals.Field(i).Elem().Set(reflect.ValueOf(assign.Unix()))
					}
				case bool:
					assign := row[colName].(bool)
					vals.Field(i).Elem().SetBool(assign)
				}
			}
		}
		wg.Wait()
		break
	}

	return nil
}
