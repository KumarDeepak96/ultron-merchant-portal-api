package Cassandra

import (
	"github.com/gocql/gocql"

	"os"
	"reflect"
	"strconv"
	"strings"
	"sync"
	"time"

	log "github.com/sirupsen/logrus"
	"ultron-merchant-portal/DataDog"
)

/*

export CASSANDRA_HOST=34.93.135.129
export CASSANDRA_KEYSPACE=user
export CASSANDRA_MAXQUEUE=200ERRO[0000] Couldn't Connect to Cassandra: gocql: unable
*/

var (
	cassandraHost = os.Getenv("CASSANDRA_HOST")
	//cassandraKeySpace       = os.Getenv("CASSANDRA_KEYSPACE")
	cassandraMaxQueue       = os.Getenv("CASSANDRA_MAXQUEUE")
	cassandraMaxRetry       = os.Getenv("CASSANDRA_MAXRETRY")
	cassandraFailedJobQueue = os.Getenv("CASSANDRA_FAILEDJOB_QUEUE")
	once                    sync.Once
	cql                     *Cassandra
)

func Instance() *Cassandra {
	once.Do(func() {
		cql = NewCQL()
	})
	return cql
}

type ORM interface {
	Update(model CQLModel) (*gocql.Query, error)
	Get(model CQLModel) error
	GetNonPrivatePost(model CQLModel) (err error)
	UpdateLoggedBatchOperations(entries []gocql.BatchEntry) error
}

type CQLModel interface {
	TableName() string
}

type Cassandra struct {
	cluster    *gocql.ClusterConfig
	Session    *gocql.Session
	Semaphore  chan struct{}
	FailedJobs chan *Job
	MaxRetry   int
	ORM        ORM
}

type Job struct {
	CQL     *Cassandra
	Payload interface{}
	Method  string
	Retry   int
}

// Wrapper over Exec method of Cassandra
func (cql *Cassandra) Exec(query string, values ...interface{}) error {

	defer func() {

	}()
	return cql.Session.Query(query, values).Exec()
}

// Wrapper over Iter method of Cassandra
func (cql *Cassandra) Iter(query string, values ...interface{}) *gocql.Iter {

	defer func() {

	}()
	return cql.Session.Query(query, values).Iter()
}

func (cql *Cassandra) NewJob(payload interface{}, method string) *Job {
	return &Job{Payload: payload, CQL: cql, Retry: 0, Method: method}
}

func (job *Job) Execute() bool {
	result := reflect.ValueOf(job.Payload).MethodByName(job.Method).Call([]reflect.Value{})
	err := result[0].Interface()

	if err != nil {
		job.Retry++
		job.CQL.FailedJobs <- job
		return false
	}
	return true
}

func (cql *Cassandra) Connect() bool {
	var err error
	connected := false
	maxRetry := 5
	retry := 0
	for connected != true {
		if cql.Session == nil || cql.Session.Closed() {
			cql.Session, err = cql.cluster.CreateSession()
			if err != nil {
				if retry == maxRetry {
					log.Panic("Cassandra connection failed")
				}
				log.Errorf("Couldn't Connect to Cassandra: %s\nRetrying in 5 seconds...attempt %d", err, retry+1)
				time.Sleep(5 * time.Second)
				retry++
			} else {
				connected = true
				log.Info("Cassandra session started...")
			}
		} else {
			connected = true
		}
	}
	return connected
}

func NewCQL() *Cassandra {
	hostString := strings.Split(cassandraHost, ",")
	maxQueue, err := strconv.Atoi(cassandraMaxQueue)
	if err != nil {
		maxQueue = 1000
	}

	maxRetry, err := strconv.Atoi(cassandraMaxRetry)
	if err != nil {
		maxRetry = 2
	}

	failedJobQueue, err := strconv.Atoi(cassandraFailedJobQueue)
	if err != nil {
		failedJobQueue = 10
	}

	cql := Cassandra{}
	cql.Semaphore = make(chan struct{}, maxQueue)
	cql.FailedJobs = make(chan *Job, failedJobQueue)
	cql.MaxRetry = maxRetry
	cql.ORM = new(CQLORM)
	cql.cluster = gocql.NewCluster(hostString...)
	//cql.cluster.Keyspace = cassandraKeySpace
	cql.cluster.Timeout = 2000 * time.Millisecond
	cql.cluster.ConnectTimeout = 2000 * time.Millisecond
	cql.cluster.RetryPolicy = &gocql.ExponentialBackoffRetryPolicy{
		NumRetries: 3,
		Min:        500 * time.Microsecond,
		Max:        6 * time.Second}

	cql.cluster.PoolConfig.HostSelectionPolicy = gocql.TokenAwareHostPolicy(gocql.RoundRobinHostPolicy())
	cql.cluster.Consistency = gocql.One

	cql.Connect()
	return &cql
}

func (cql *Cassandra) ProcessFailedJobs() {
	datadog := DataDog.Instance()
	for {
		select {
		case job := <-cql.FailedJobs:
			if cql.Connect() {
				if job.Retry <= cql.MaxRetry {
					log.Warnf("Executing Failed Job %s: Attempt: %d, Method: %s, Data: %+v",
						reflect.TypeOf(job.Payload).Name(), job.Retry, job.Method, job.Payload)
					job.Execute()
					datadog.Count("consumer.cassandra.failedjobs", 1, strings.Fields("executed"), 1)
				} else {
					log.Errorf("Job permanently failed %s: Data: %v, Method: %s",
						reflect.TypeOf(job.Payload).Name(), job.Payload, job.Method)
					datadog.Count("consumer.cassandra.failedjobs", 1, strings.Fields("failed"), 1)
				}
			}
		}
	}
}
