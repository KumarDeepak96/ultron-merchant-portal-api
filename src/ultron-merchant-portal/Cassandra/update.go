package Cassandra

import (
	"fmt"
	"ultron-merchant-portal/DataDog"
	"reflect"
	"strconv"
	"strings"
	"time"

	inf "gopkg.in/inf.v0"

	"github.com/gocql/gocql"
	log "github.com/sirupsen/logrus"
)

type CQLError struct {
	message string
}

func (c CQLError) Error() string {
	return c.message
}

func PartialUpdate(model CQLModel) (query *gocql.Query, err error) {

	defer func() {
		if e := recover(); e != nil {
			log.Errorf("Panic happened in Cassandra.PartialUpdate: %+v", e)
			err = CQLError{message: fmt.Sprintf("panic: %+v", e)}
		}
	}()

	queryStr := `UPDATE ` + model.TableName() + ` SET`

	vals := reflect.ValueOf(model)

	if vals.Kind() == reflect.Ptr {
		vals = vals.Elem()
	}

	setterStrs := []string{}
	queryArgs := []interface{}{}

	pkNames := []string{}
	pkVars := []interface{}{}

	for i := 0; i < vals.NumField(); i++ {
		rt, rv := vals.Field(i).Type().Kind(), vals.Field(i).Interface()

		isPointer := rt == reflect.Ptr
		if isPointer {
			rt = vals.Field(i).Type().Elem().Kind()
		}

		tag := strings.Split(vals.Type().Field(i).Tag.Get("cql"), ",")

		if len(tag) == 0 {
			continue
		}

		colName := tag[0]

		if colName == "" {
			continue
		}

		var colType string
		if len(tag) > 1 {
			colType = tag[1]
		}

		if len(tag) > 2 && tag[2] == "primarykey" {
			if !isFieldValid(vals.Field(i)) {
				return nil, CQLError{message: "Primary Key cannot be nil"}
			}

			pkNames = append(pkNames, colName)
			pkVars = append(pkVars, rv)

			continue
		}

		if !isFieldValid(vals.Field(i)) {
			continue
		}

		switch colType {
		case "timestamp":
			rv, err = marshalTimestamp(vals.Field(i))
			if err != nil {
				return nil, err
			}
		case "decimal":
			rv, err = marshalDecimal(vals.Field(i))
			if err != nil {
				return nil, err
			}
		case "int":
			rv, _ = marshalIntFromBool(vals.Field(i))
		case "tuple":
			if rt != reflect.Array && rt != reflect.Slice {
				continue
			}

			l := vals.Field(i).Len()
			var holders []string
			for j := 0; j < l; j++ {
				holders = append(holders, "?")
				queryArgs = append(queryArgs, vals.Field(i).Index(j).Interface())
			}

			setter := fmt.Sprintf(" %s = ( ", colName) + strings.Join(holders, ",") + " ) "

			setterStrs = append(setterStrs, setter)

			continue
		}

		setterStrs = append(setterStrs, fmt.Sprintf(" %s = ? ", colName))
		queryArgs = append(queryArgs, rv)
	}

	if len(pkNames) == 0 {
		return nil, CQLError{message: "No Primary Key fields set on struct"}
	}

	var whereClause []string

	for _, pk := range pkNames {
		whereClause = append(whereClause, fmt.Sprintf("%s = ?", pk))
	}

	queryStr = queryStr + strings.Join(setterStrs, ",") + `WHERE ` + strings.Join(whereClause, " AND ")

	queryArgs = append(queryArgs, pkVars...)

	query = cql.Session.Query(queryStr, queryArgs...)

	return
}

func GetPartialUpdateQuery(model CQLModel) (q string, values []interface{}, err error) {
	datadog := DataDog.Instance()
	var query *gocql.Query
	defer func() {

		if e := recover(); e != nil {
			log.Errorf("Panic happened in Cassandra.PartialUpdate: %+v", e)
			log.Info("paniced query is ", query.String())
			err = CQLError{message: fmt.Sprintf("panic: %+v", e)}
			datadog.Count("consumer.cassandra.write.fail", 1, strings.Fields("partial_update"), 1)
		}
	}()

	queryStr := `UPDATE ` + model.TableName() + ` SET`

	vals := reflect.ValueOf(model)

	if vals.Kind() == reflect.Ptr {
		vals = vals.Elem()
	}

	setterStrs := []string{}
	queryArgs := []interface{}{}

	pkNames := []string{}
	pkVars := []interface{}{}

	for i := 0; i < vals.NumField(); i++ {
		rt, rv := vals.Field(i).Type().Kind(), vals.Field(i).Interface()

		isPointer := rt == reflect.Ptr
		if isPointer {
			rt = vals.Field(i).Type().Elem().Kind()
		}

		tag := strings.Split(vals.Type().Field(i).Tag.Get("cql"), ",")

		if len(tag) == 0 {
			continue
		}

		colName := tag[0]

		if colName == "" {
			continue
		}

		var colType string
		if len(tag) > 1 {
			colType = tag[1]
		}

		if len(tag) > 2 && tag[2] == "primarykey" {
			if !isFieldValid(vals.Field(i)) {
				err = CQLError{message: "Primary Key cannot be nil"}
				return
			}

			pkNames = append(pkNames, colName)
			pkVars = append(pkVars, rv)

			continue
		}

		if !isFieldValid(vals.Field(i)) {
			continue
		}

		switch colType {
		case "timestamp":
			rv, err = marshalTimestamp(vals.Field(i))
			if err != nil {
				return
			}
		case "decimal":
			rv, err = marshalDecimal(vals.Field(i))
			if err != nil {
				return
			}
		case "int":
			rv, _ = marshalIntFromBool(vals.Field(i))
		case "tuple":
			if rt != reflect.Array && rt != reflect.Slice {
				continue
			}

			l := vals.Field(i).Len()
			var holders []string
			for j := 0; j < l; j++ {
				holders = append(holders, "?")
				queryArgs = append(queryArgs, vals.Field(i).Index(j).Interface())
			}

			setter := fmt.Sprintf(" %s = ( ", colName) + strings.Join(holders, ",") + " ) "

			setterStrs = append(setterStrs, setter)

			continue
		}

		setterStrs = append(setterStrs, fmt.Sprintf(" %s = ? ", colName))
		queryArgs = append(queryArgs, rv)
	}

	if len(pkNames) == 0 {
		err = CQLError{message: "No Primary Key fields set on struct"}
		return
	}

	var whereClause []string

	for _, pk := range pkNames {
		whereClause = append(whereClause, fmt.Sprintf("%s = ?", pk))
	}

	queryStr = queryStr + strings.Join(setterStrs, ",") + `WHERE ` + strings.Join(whereClause, " AND ")

	queryArgs = append(queryArgs, pkVars...)

	query = cql.Session.Query(queryStr, queryArgs...)
	q = queryStr
	values = queryArgs

	return
}

func marshalTimestamp(v reflect.Value) (interface{}, error) {
	rt, rv := v.Type().Kind(), v.Interface()

	isPointer := rt == reflect.Ptr
	if isPointer {
		rt = v.Type().Elem().Kind()
	}

	switch rt {
	case reflect.Int64:
		var v int64
		if isPointer {
			v = *rv.(*int64)
		} else {
			v = rv.(int64)
		}
		rv = time.Unix(int64(v/1000), 0)
	case reflect.String:
		var v string
		if isPointer {
			v = *rv.(*string)
		} else {
			v = rv.(string)
		}
		t, err := time.Parse(time.RFC3339, v)

		if err != nil {
			return nil, err
		}

		rv = t
	}

	return rv, nil
}

func marshalDecimal(v reflect.Value) (interface{}, error) {
	rt, rv := v.Type().Kind(), v.Interface()

	isPointer := rt == reflect.Ptr
	if isPointer {
		rt = v.Type().Elem().Kind()
	}

	dec := new(inf.Dec)

	switch rt {
	case reflect.Float64:
		var v float64
		if isPointer {
			v = *rv.(*float64)
		} else {
			v = rv.(float64)
		}

		dec.SetString(strconv.FormatFloat(v, 'f', 2, 64))
	case reflect.Int, reflect.Int64:
		var v float64

		if rt == reflect.Int {
			if isPointer {
				v = float64(*rv.(*int))
			} else {
				v = float64(rv.(int))
			}
		} else {
			if isPointer {
				v = float64(*rv.(*int64))
			} else {
				v = float64(rv.(int64))
			}
		}

		dec.SetString(strconv.FormatFloat(v, 'f', 2, 64))
	case reflect.String:
		var v string
		if isPointer {
			v = *rv.(*string)
		} else {
			v = rv.(string)
		}
		dec.SetString(v)
	default:
		return nil, CQLError{message: "decimal field not one of types: int, int64, float64, string"}
	}

	rv = dec

	return rv, nil
}

func marshalIntFromBool(v reflect.Value) (interface{}, error) {
	rt, rv := v.Type().Kind(), v.Interface()

	isPointer := rt == reflect.Ptr
	if isPointer {
		rt = v.Type().Elem().Kind()
	}

	if rt != reflect.Bool {
		return rv, CQLError{message: "not a bool"}
	}

	var b bool
	if isPointer {
		b = *rv.(*bool)
	} else {
		b = rv.(bool)
	}

	if b {
		rv = 1
	} else {
		rv = 0
	}

	return rv, nil
}

func isFieldValid(v reflect.Value) bool {
	switch v.Type().Kind() {
	case reflect.Chan, reflect.Func, reflect.Slice, reflect.Interface, reflect.Map, reflect.Ptr:
		if v.IsNil() {
			return false
		}
	default:
		if !v.IsValid() {
			return false
		}
	}

	return true
}

func (o *CQLORM) UpdateLoggedBatchOperations(batchEnties []gocql.BatchEntry) error {
	batch := cql.Session.NewBatch(gocql.LoggedBatch)
	batch.SetConsistency(gocql.One)
	for _, b := range batchEnties {
		batch.Entries = append(batch.Entries, b)
	}

	return executeBatch(batch)
}

func executeBatch(batch *gocql.Batch) error {
	err := cql.Session.ExecuteBatch(batch)
	if err != nil {
		log.WithFields(log.Fields{
			"err": err,
		}).Error("error in executing batch ", batch.Entries[0].Stmt)
	}
	return err
}
